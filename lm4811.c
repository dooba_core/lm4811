/* Dooba SDK
 * LM4811 Stereo Headphone Amplifier Driver
 */

// External Includes
#include <util/delay.h>
#include <dio/dio.h>

// Internal Includes
#include "lm4811.h"

// Pins
uint8_t lm4811_pin_shdn;
uint8_t lm4811_pin_clk;
uint8_t lm4811_pin_vupdn;

// Initialize
void lm4811_init(uint8_t shdn_pin, uint8_t clk_pin, uint8_t vupdn_pin)
{
	uint8_t i;

	// Pre-init
	lm4811_preinit(shdn_pin, clk_pin, vupdn_pin);

	// Setup Pins
	lm4811_pin_shdn = shdn_pin;
	lm4811_pin_clk = clk_pin;
	lm4811_pin_vupdn = vupdn_pin;
	dio_lo(lm4811_pin_shdn);
	dio_lo(lm4811_pin_clk);
	dio_lo(lm4811_pin_vupdn);

	// Zero Volume
	for(i = 0; i < LM4811_VOL_STEPS; i = i + 1)							{ lm4811_vol_down(); }
}

// Pre-Initialize
void lm4811_preinit(uint8_t shdn_pin, uint8_t clk_pin, uint8_t vupdn_pin)
{
	// Configure Pins as Outputs
	dio_output(shdn_pin);
	dio_output(clk_pin);
	dio_output(vupdn_pin);
}

// Volume Up
void lm4811_vol_up()
{
	// Clock it
	dio_hi(lm4811_pin_vupdn);
	dio_hi(lm4811_pin_clk);
	_delay_us(LM4811_DELAY_VUPDN);
	dio_lo(lm4811_pin_clk);
}

// Volume Down
void lm4811_vol_down()
{
	// Clock it
	dio_lo(lm4811_pin_vupdn);
	dio_hi(lm4811_pin_clk);
	_delay_us(LM4811_DELAY_VUPDN);
	dio_lo(lm4811_pin_clk);
}

// Mute
void lm4811_mute()
{
	// Enter Shutdown Mode
	dio_hi(lm4811_pin_shdn);

	// Delay
	_delay_us(LM4811_DELAY_SHDN);
}

// Unmute
void lm4811_unmute()
{
	// Pull out of Shutdown Mode
	dio_lo(lm4811_pin_shdn);

	// Delay
	_delay_us(LM4811_DELAY_SHDN);
}
