/* Dooba SDK
 * LM4811 Stereo Headphone Amplifier Driver
 */

#ifndef	__LM4811_H
#define	__LM4811_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Delays
#define	LM4811_DELAY_SHDN			100
#define	LM4811_DELAY_VUPDN			100

// Volume Steps
#define	LM4811_VOL_STEPS			16

// Initialize
extern void lm4811_init(uint8_t shdn_pin, uint8_t clk_pin, uint8_t vupdn_pin);

// Pre-Initialize
extern void lm4811_preinit(uint8_t shdn_pin, uint8_t clk_pin, uint8_t vupdn_pin);

// Volume Up
extern void lm4811_vol_up();

// Volume Down
extern void lm4811_vol_down();

// Mute
extern void lm4811_mute();

// Unmute
extern void lm4811_unmute();

#endif
